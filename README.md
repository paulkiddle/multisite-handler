# Multisite Request Handler

A simple library for routing an incomming message to different handlers based on the hostname in the request's Host header.

```javascript
const http = require('http');
const multisite = require('multisite-hander');

const siteOne = (req, res) => res.end('Welcome to kittens.example, home of kitten things!')
const siteTwo = (req, res) => res.end('Welcome to anarchism.example, home of anarchist things!')

// Provide a dict object to map your domain names to site handlers
const handler = multisite({
	'kittens.example': siteOne,
	'anarchism.example': siteTwo
})

http.createServer(handler).listen(1234);
```

Also accepts an async function instead of a dict, so you can write more complex matchers
(or even something async like looking names up in a database).

```javascript
const handler = multisite(hostname => {
	// If the host header has a port specified, it will be stripped before this function is called
	if(hostname.match(/\bkittens\.example$/)) {
		return kittenSite;
	}
})
```
