const multisite = require('../src/index.js');

test('Parses req header for host', async ()=>{
	const site = jest.fn();
	const getSite = jest.fn(host=>site);
	const handle = multisite(getSite);

	const req = {
		headers: {
			host: 'test-host:2345'
		}
	}
	const res = {}

	await handle(req, res);

	expect(getSite).toHaveBeenCalledWith('test-host');
	expect(site).toHaveBeenCalledWith(req, res);
})

test('Calls next function if site not found', async () =>{
	const next = jest.fn();

	const handle = multisite({}, next);

	const req = {
		headers: {
			host: 'test-host:2345'
		}
	}
	const res = {}

	await handle(req, res);
	expect(next).toHaveBeenCalledWith(req, res);
});

test('Writes error message to res on error', async ()=>{
	const res = {
		end: jest.fn()
	}
	const e = {error:'error'}
	const consoleError = console.error;
	console.error = jest.fn();

	const req = { headers: { host:'' }}

	const handler = multisite(h=>{ throw e; });

	await handler(req, res);

	expect(console.error).toHaveBeenCalledWith(e);
	expect(res.statusCode).toBe(500);
	expect(res.end.mock.calls).toMatchSnapshot()

	console.error = consoleError
})
