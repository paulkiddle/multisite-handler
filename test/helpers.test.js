const {siteGetter, notFound, error} = require('../src/helpers.js');

test('Creates site getter from object', ()=>{
	const o = {
		a: Symbol('host a')
	}

	const getSite = siteGetter(o);

	expect(getSite('a')).toBe(o.a);
	expect(getSite('B')).toBe(null);
});

test('Writes not found message to res', ()=>{
	const res = {
		end: jest.fn()
	}

	notFound(null, res);

	expect(res.statusCode).toBe(404);
	expect(res.end.mock.calls).toMatchSnapshot()
})
