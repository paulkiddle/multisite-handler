const {siteGetter, notFound, error} = require('./helpers')

/**
 * Create the multisite handler. The first argument is a dict or object that maps hostnames to
 * handler functions.
 *
 * @param {Function|Object} getSite Dict or function mapping hostnames to request handler functions
 * @param {Function} next Fallback handler for sites that aren't handled by getSite
 * @returns {Function} Site handler of the form `handler(req, res)`
 */
function main(getSite, next = notFound) {
	if(typeof getSite !== 'function') {
		getSite = siteGetter(getSite);
	}

	const handle = async (req, res) => {
		const hostname = req.headers.host.split(':')[0];

		const site = await getSite(hostname);

		if(site) {
			return site(req, res);
		} else {
			return next(req, res);
		}
	}

	return async(req, res)=>{
		try {
			return await handle(req, res);
		} catch(e) {
			console.error(e)
			return error(req, res);
		}
	}
}

module.exports = main;
