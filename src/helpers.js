/**
 * Turns a dict object into a function that returns a site handler function when passed a domain name
 * @param {Object} sites Dict mapping hostnames to handler functions acceptiing (req, res) args
 * @returns	Function accepting `hostname` as an argument and returning `handler(req, res)`
 */
function siteGetter(sites){
	return hostname => {
		const hosts = Object.getOwnPropertyNames(sites);

		return hosts.includes(hostname) ? sites[hostname] : null;
	}
}

/**
 * Sends a 404 response
 * @param {http.IncomingMessage} req IncomingMessage
 * @param {http.ServerResponse} res Response
 */
function notFound(req, res) {
	res.statusCode = 404;
	res.end(`I don't know what you're looking for, but it's not here.`);
}

/**
 * Sends a 500 error message
 * @param {http.IncomingMessage} req Incomming message
 * @param {http.ServerResponse} res Response message
 */
function error(req, res) {
	res.statusCode = 500;
	res.end(`Something has gone wrong. That's all the information I can give you, sorry.`);
}

module.exports = { siteGetter, notFound, error}
