
<a name="main"></a>

## main(getSite, next) ⇒ <code>function</code>
Create the multisite handler. The first argument is a dict or object that maps hostnames to
handler functions.

**Kind**: global function  
**Returns**: <code>function</code> - Site handler of the form `handler(req, res)`  

| Param | Type | Description |
| --- | --- | --- |
| getSite | <code>function</code> \| <code>Object</code> | Dict or function mapping hostnames to request handler functions |
| next | <code>function</code> | Fallback handler for sites that aren't handled by getSite |

## Functions

<dl>
<dt><a href="#siteGetter">siteGetter(sites)</a> ⇒</dt>
<dd><p>Turns a dict object into a function that returns a site handler function when passed a domain name</p>
</dd>
<dt><a href="#notFound">notFound(req, res)</a></dt>
<dd><p>Sends a 404 response</p>
</dd>
<dt><a href="#error">error(req, res)</a></dt>
<dd><p>Sends a 500 error message</p>
</dd>
</dl>

<a name="siteGetter"></a>

## siteGetter(sites) ⇒
Turns a dict object into a function that returns a site handler function when passed a domain name

**Kind**: global function  
**Returns**: Function accepting `hostname` as an argument and returning `handler(req, res)`  

| Param | Type | Description |
| --- | --- | --- |
| sites | <code>Object</code> | Dict mapping hostnames to handler functions acceptiing (req, res) args |

<a name="notFound"></a>

## notFound(req, res)
Sends a 404 response

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| req | <code>http.IncomingMessage</code> | IncomingMessage |
| res | <code>http.ServerResponse</code> | Response |

<a name="error"></a>

## error(req, res)
Sends a 500 error message

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| req | <code>http.IncomingMessage</code> | Incomming message |
| res | <code>http.ServerResponse</code> | Response message |

